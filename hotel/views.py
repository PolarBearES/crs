from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from .serializers import *


class HotelViewSet(ModelViewSet):
    @method_decorator(cache_page(60))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @method_decorator(cache_page(60))
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    queryset = Hotel.objects.all()

    serializer_class = HotelSerializer


class RoomViewSet(ModelViewSet):
    @method_decorator(cache_page(60))
    def list(self, *args, **kwargs):
        return super().list(*args, **kwargs)

    @method_decorator(cache_page(60))
    def retrieve(self, *args, **kwargs):
        return super().retrieve(*args, **kwargs)

    def get_serializer_context(self):
        return {'hotel_pk': self.kwargs['hotel_pk']}

    def get_queryset(self):
        return Room.objects.filter(hotel__pk=self.kwargs['hotel_pk']).all()

    serializer_class = RoomSerializer


class RateViewSet(ModelViewSet):
    def get_serializer_context(self):
        return {'room_pk': self.kwargs['room_pk']}

    def get_queryset(self):
        return Rate.objects.filter(room__pk=self.kwargs['room_pk']).all()

    serializer_class = RateSerializer


class InventoryViewSet(ModelViewSet):
    def get_serializer_context(self):
        return {'rate_pk': self.kwargs['rate_pk']}

    def get_queryset(self):
        return Inventory.objects.filter(rate__pk=self.kwargs['rate_pk']).all()

    serializer_class = InventorySerializer


@api_view(['GET'])
@permission_classes([AllowAny])
def availability(request, hotel_id, checkin_date, checkout_date):
    hotel = Rate.objects.select_related('room') \
        .prefetch_related('inventory_set') \
        .filter(room__hotel_id=hotel_id)\
        .filter(date__gte=checkin_date,date__lte=checkout_date)\


    serializer = AvailabilityRateSerializer(hotel, many=True)
    return Response(serializer.data)

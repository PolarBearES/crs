from django.db.models.fields import related_lookups
from rest_framework import serializers

from .models import *


class HotelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hotel
        fields = ['id', 'name']


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['id', 'name', 'hotel_id']

    def create(self, validated_data):
        hotel_pk = self.context['hotel_pk']
        return Room.objects.create(hotel_id=hotel_pk, **validated_data)


class RateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rate
        fields = ['id', 'name', 'date']

    def create(self, validated_data):
        room_pk = self.context['room_pk']
        return Rate.objects.create(room_id=room_pk, **validated_data)


class InventorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Inventory
        fields = ['id', 'price', 'allotment']

    def create(self, validated_data):
        rate_pk = self.context['rate_pk']
        return Inventory.objects.create(rate_id=rate_pk, **validated_data)

class AvailabilityInventorySerializer(InventorySerializer):
    class Meta:
        model = Inventory
        fields = ['price', 'allotment']

class AvailabilityRateSerializer(RateSerializer):
    breakdown = AvailabilityInventorySerializer(source='inventory_set', many=True, read_only=True)
    total_price = serializers.SerializerMethodField()

    def get_total_price(self, rate):
        return sum([item.price for item in rate.inventory_set.all()])

    class Meta:
        model = Rate
        fields = ['name','date','total_price', 'breakdown']





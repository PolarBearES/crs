from django.contrib import admin

# Register your models here.
from .models import *


@admin.register(Hotel)
class HotelAdmin(admin.ModelAdmin):
    pass


@admin.register(Room)
class RoomAdmin(admin.ModelAdmin):
    pass


class InventoryInline(admin.TabularInline):
    model = Inventory


@admin.register(Rate)
class RateAdmin(admin.ModelAdmin):
    inlines = [InventoryInline]

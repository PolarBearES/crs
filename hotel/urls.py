from datetime import datetime

from django.urls import path, include, register_converter
from rest_framework import routers
from rest_framework_nested import routers
from .views import *

router = routers.DefaultRouter()
router.register('hotels', HotelViewSet)

rooms_router = routers.NestedDefaultRouter(router, 'hotels', lookup='hotel')
rooms_router.register('rooms', RoomViewSet, basename='rooms')

rates_router = routers.NestedSimpleRouter(rooms_router, 'rooms', lookup='room')
rates_router.register('rates', RateViewSet, basename='rates')

inventory_router = routers.NestedSimpleRouter(rates_router, 'rates', lookup='rate')
inventory_router.register('inventory', InventoryViewSet, basename='inventory')


class DateConverter:
    regex = '\d{4}-\d{2}-\d{2}'

    def to_python(self, value):
        return datetime.strptime(value, '%Y-%m-%d')

    def to_url(self, value):
        return value


register_converter(DateConverter, 'date')

urlpatterns = [
    path(r'', include(router.urls)),
    path(r'', include(rooms_router.urls)),
    path(r'', include(rates_router.urls)),
    path(r'', include(inventory_router.urls)),
    path('availability/<str:hotel_id>/<date:checkin_date>/<date:checkout_date>/', availability, ),
]

from django.db import models


# Create your models here.
class Hotel(models.Model):
    id = models.CharField(primary_key=True, max_length=225)
    name = models.CharField(max_length=225)

    def __str__(self):
        return self.name


class Room(models.Model):
    id = models.CharField(primary_key=True, max_length=225)
    name = models.CharField(max_length=225)
    hotel = models.ForeignKey(Hotel, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.hotel} - {self.name}'


class Rate(models.Model):
    id = models.CharField(primary_key=True, max_length=225)
    name = models.CharField(max_length=225)
    date = models.DateField()
    room = models.ForeignKey(Room, on_delete=models.PROTECT)

    def __str__(self):
        return self.name


class Inventory(models.Model):
    price = models.DecimalField(max_digits=8, decimal_places=2)
    allotment = models.PositiveIntegerField()
    rate = models.ForeignKey(Rate, on_delete=models.PROTECT)

    def __str__(self):
        return f'{self.rate} - S{self.price}€ ({self.allotment})'


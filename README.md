Simply run docker-compose up -d to run the entire project.


To apply migrations
```docker exec crs_django_1 python manage.py migrate```

To create superuser
```docker exec -it crs_django_1 python manage.py createsuperuser```

Go to http://localhost:8000/
